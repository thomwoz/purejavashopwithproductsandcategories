package com.company;

import java.util.Scanner;

public class InputDataFromUser {
    private Scanner scanner = new Scanner(System.in);

    public Category createCategory() {
        System.out.println("podaj nazwę kategorii: ");
        String categoryNameLocal = scanner.next();
        return new Category(categoryNameLocal);

    }

    public Product createProduct() {
        System.out.println("podaj nazwę: ");
        String name = scanner.next();
        System.out.println("podaj opis: ");
        String desc = scanner.next();
        System.out.println("podaj cenę: ");
        double price = (scanner.nextDouble());
        System.out.println("podaj ilość: ");
        int quantity = scanner.nextInt();

        return new Product(name, desc, price, quantity);

    }

    public int chooseOpiton() {
        System.out.println("Wprowadź co chcesz zrobić: ");
        System.out.println("1 dodaj kategorie, 2 Dodaj produkt, 3 wyświetl kategorie, 4 wyświetl produkty" +
                ", 5 Wyświetl produkty z kategorii, 6  skasuj produkt, 7 kasuj kategorie, 8 wyjdź");

        return  scanner.nextInt();
    }
    public String inputCategoryName(){
        System.out.println("Podaj nazwe kategorii");
        return scanner.next();
    }
    public String inputName(){
        System.out.println("podaj nazwe produktu: ");
        return scanner.next();
    }
    public int inputQuantity(){
        System.out.println("wprowadź ile usunąć");
        return scanner.nextInt();
    }

}
