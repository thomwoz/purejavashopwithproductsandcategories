package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class CategoryList {
    private List<Category> categoryLists = new ArrayList<>();

    public void addCategory(Category category){
        boolean test = false ;
        for(Category a: categoryLists){
            if(a.getCategoryName().equals(category.getCategoryName())){
                System.out.println("Kategoria już jest");
                test = true;
                break;
            }
        }
        if(!test){
            categoryLists.add(category);
        }

    }

    public void addProduct(Product product, String categoryName){
        boolean hasFound=false;
        for(Category a: categoryLists){
            if(categoryName.equals(a.getCategoryName())){
                System.out.println("Brak Kategorii dodajemy nową kategorie");
                a.getProducts().add(product);
                hasFound = true;
                break;
            }
        }
        if(!hasFound){
            Category c = new Category(categoryName);
            c.getProducts().add(product);
            categoryLists.add(c);
        }
    }

    public void eraseCategory(String categoryName){
        Iterator<Category> categoryIterator = categoryLists.listIterator();
        while(categoryIterator.hasNext()){
            if(categoryName.equals(categoryIterator.next().getCategoryName())){
                categoryIterator.remove();
                break;
            }
        }
    }

    public void eraseProduct(String categoryName, String productName, int quantity){
        boolean hasErased = false;
        Product p;
        for(Category c:categoryLists){
            if(categoryName.equals(c.getCategoryName())){

                for(Product product:c.getProducts()){
                    if(product.getName().equals(productName)){
                        p=product;
                        p.setQuantity(p.getQuantity()-quantity);
                        if(p.getQuantity()==0){
                            c.getProducts().remove(p);
                            System.out.println("produkt usunięty");
                            break;
                        }
                        hasErased = true;
                        System.out.println("Ilość zmodyfikowana");
                        break;
                    }
                }

            }
            if(hasErased){
                break;
            }
        }
    }

    public void showCategorys(){
        System.out.println("lista kategorii: ");
        for(Category a: categoryLists){
            System.out.println(" - "+a.getCategoryName());
        }
    }

    public void showAllProducts(){
        for(Category a: categoryLists){
            System.out.println("");
            System.out.println("Nazwa kategorii " + a.getCategoryName());
            for(Product b: a.getProducts()){
                System.out.print(b);
            }
        }
    }

    public void showAllFromCategory(String categoryName){
        Category cat=new Category();
        for(Category c: categoryLists){
            if(categoryName.equals(c.getCategoryName())){
                cat=c;
                break;
            }
        }
        for (Product b: cat.getProducts()){
            System.out.println(b);
        }
    }

}
