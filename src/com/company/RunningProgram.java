package com.company;

public class RunningProgram {
    private InputDataFromUser sc = new InputDataFromUser();
    private CategoryList shop = new CategoryList();


    public void runProgram() {
        int choice = 2;
        shop.addCategory(new Category("aaa"));
        shop.addProduct(new Product("sss", "dsa fdaf daf ", 23, 33), "aaa");
        while (choice != 8) {
            choice = sc.chooseOpiton();
            switch (choice) {
                case 1: {
                    shop.addCategory(sc.createCategory());
                    break;
                }
                case 2: {
                    shop.addProduct(sc.createProduct(), sc.inputCategoryName());
                    break;
                }
                case 3: {
                    shop.showCategorys();
                    break;
                }
                case 4: {
                    shop.showAllProducts();
                    break;
                }
                case 5: {
                    shop.showAllFromCategory(sc.inputCategoryName());
                    break;
                }
                case 6: {
                    shop.eraseProduct(sc.inputCategoryName(), sc.inputName(), sc.inputQuantity());
                    break;
                }
                case 7: {
                    shop.eraseCategory(sc.inputCategoryName());
                    break;
                }
                case 8: {
                    System.out.println("Dziękujemy za skorzystanie z programu.");
                }
                default:
                    System.out.println("Brak takiej opcji");
                    ;
            }
        }
    }
}

